-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour contact
CREATE DATABASE IF NOT EXISTS `contact` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `contact`;

-- Listage de la structure de la table contact. admins
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Listage des données de la table contact.admins : ~1 rows (environ)
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` (`id`, `pseudo`, `password`) VALUES
	(1, 'admin', '240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;

-- Listage de la structure de la table contact. messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `email` varchar(255) NOT NULL,
  `question` text NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Listage des données de la table contact.messages : ~4 rows (environ)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` (`id`, `name`, `email`, `question`, `read`) VALUES
	(8, 'admin', 'admin@gmail.com', 'why ??', 0),
	(9, 'admin123', 'admin@gmail.com', 'why ??', 1),
	(10, 'Juju', 'juju@gmail.com', '\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur luctus enim elit, vitae consequat ipsum euismod facilisis. Suspendisse tempor aliquet felis eget tempus. Aenean vel neque eget purus finibus consectetur et sit amet felis. Maecenas blandit arcu tortor, sit amet rhoncus tellus porta id. Suspendisse potenti.', 0),
	(11, 'nemo', 'nemo@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et scelerisque odio. Sed iaculis a metus maximus aliquet. Proin eget arcu nec purus consectetur pharetra posuere vitae tortor.', 0);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
