/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/ts/server.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/ts/bdd.ts":
/*!***********************!*\
  !*** ./app/ts/bdd.ts ***!
  \***********************/
/*! exports provided: connection */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "connection", function() { return connection; });
/* harmony import */ var mysql__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mysql */ "mysql");
/* harmony import */ var mysql__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mysql__WEBPACK_IMPORTED_MODULE_0__);

var connection = mysql__WEBPACK_IMPORTED_MODULE_0__["createConnection"]({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'contact',
    port: 3306
});


/***/ }),

/***/ "./app/ts/server.ts":
/*!**************************!*\
  !*** ./app/ts/server.ts ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! body-parser */ "body-parser");
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sha256__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sha256 */ "sha256");
/* harmony import */ var sha256__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sha256__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _bdd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bdd */ "./app/ts/bdd.ts");




var port = 9000;
var app = express__WEBPACK_IMPORTED_MODULE_0__();
// Permet de gérer les access au serveurs
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, PATCH, POST, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        //respond with 200
        res.send(200);
    }
    else {
        //move on
        next();
    }
});
app.use(body_parser__WEBPACK_IMPORTED_MODULE_1__["urlencoded"]({ extended: true }));
app.use(body_parser__WEBPACK_IMPORTED_MODULE_1__["json"]());
// Insert un nouveau message
app.post('/contact', function (request, response) {
    var name = request.body.name;
    var email = request.body.email;
    var question = request.body.question;
    var sql = 'INSERT INTO messages (name, email, question) VALUES(?, ?, ?)';
    _bdd__WEBPACK_IMPORTED_MODULE_3__["connection"].query(sql, [name, email, question], function (error, result, fields) {
        if (error)
            throw error;
        if (result)
            return response.send(JSON.stringify({ "great": "Votre message a bien été envoyer" }));
    });
});
// Vérifie si l'user et bien existant
app.post('/login', function (request, response) {
    var pseudo = request.body.pseudo;
    var password = request.body.password;
    var passwordHash = sha256__WEBPACK_IMPORTED_MODULE_2__(password);
    var sql = 'SELECT * FROM admins WHERE pseudo=?';
    _bdd__WEBPACK_IMPORTED_MODULE_3__["connection"].query(sql, [pseudo, passwordHash], function (error, results, fields) {
        if (error)
            throw error;
        if (results.length > 0) {
            for (var _i = 0, results_1 = results; _i < results_1.length; _i++) {
                var result = results_1[_i];
                if (result.password === passwordHash) {
                    return response.send(JSON.stringify(result));
                }
            }
        }
        else {
            return response.send(JSON.stringify({ "error": "mot de passe ou pseudo incorrect" }));
        }
    });
});
// Envoie tout les messages
app.get('/contact/messages', function (request, response) {
    var sql = 'SELECT * FROM messages';
    _bdd__WEBPACK_IMPORTED_MODULE_3__["connection"].query(sql, function (error, results, fields) {
        if (error)
            throw error;
        if (results)
            return response.send(JSON.stringify(results));
    });
});
// Mets a jour le message en spécifiant qu'il a était lu
app.patch('/contact/:id', function (request, response) {
    var id = parseInt(request.params['id']);
    var read = request.body.read;
    var sql = 'UPDATE messages SET `read` = ? WHERE id = ?';
    _bdd__WEBPACK_IMPORTED_MODULE_3__["connection"].query(sql, [read, id], function (error, result, fields) {
        if (error)
            throw error;
        if (result)
            return response.send(JSON.stringify({ "great": "mail checked" }));
    });
});
app.listen(port, function () { console.log("Listening on port " + port); });


/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "mysql":
/*!************************!*\
  !*** external "mysql" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mysql");

/***/ }),

/***/ "sha256":
/*!*************************!*\
  !*** external "sha256" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sha256");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYXBwL3RzL2JkZC50cyIsIndlYnBhY2s6Ly8vLi9hcHAvdHMvc2VydmVyLnRzIiwid2VicGFjazovLy9leHRlcm5hbCBcImJvZHktcGFyc2VyXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZXhwcmVzc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm15c3FsXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwic2hhMjU2XCIiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUErQjtBQUV4QixJQUFNLFVBQVUsR0FBRyxzREFBc0IsQ0FBQztJQUM3QyxJQUFJLEVBQUUsV0FBVztJQUNqQixJQUFJLEVBQUUsTUFBTTtJQUNaLFFBQVEsRUFBRSxFQUFFO0lBQ1osUUFBUSxFQUFFLFNBQVM7SUFDbkIsSUFBSSxFQUFFLElBQUk7Q0FDYixDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7QUNSSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQW1DO0FBQ087QUFDVDtBQUNBO0FBRWpDLElBQU0sSUFBSSxHQUFXLElBQUksQ0FBQztBQUUxQixJQUFNLEdBQUcsR0FBd0Isb0NBQU8sRUFBRSxDQUFDO0FBRTNDLHlDQUF5QztBQUN6QyxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVMsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJO0lBQzNCLEdBQUcsQ0FBQyxNQUFNLENBQUMsNkJBQTZCLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDL0MsR0FBRyxDQUFDLE1BQU0sQ0FBQyw4QkFBOEIsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO0lBQ3hFLEdBQUcsQ0FBQyxNQUFNLENBQUMsOEJBQThCLEVBQUUsZ0RBQWdELENBQUMsQ0FBQztJQUM3RiwyQkFBMkI7SUFDM0IsSUFBSSxTQUFTLEtBQUssR0FBRyxDQUFDLE1BQU0sRUFBRTtRQUMxQixrQkFBa0I7UUFDbEIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUNqQjtTQUNJO1FBQ0QsU0FBUztRQUNULElBQUksRUFBRSxDQUFDO0tBQ1Y7QUFDTCxDQUFDLENBQUMsQ0FBQztBQUNILEdBQUcsQ0FBQyxHQUFHLENBQUMsc0RBQXFCLENBQUMsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ2pELEdBQUcsQ0FBQyxHQUFHLENBQUMsZ0RBQWUsRUFBRSxDQUFDLENBQUM7QUFFM0IsNEJBQTRCO0FBQzVCLEdBQUcsQ0FBQyxJQUFJLENBQUUsVUFBVSxFQUFFLFVBQUUsT0FBd0IsRUFBRSxRQUEwQjtJQUN4RSxJQUFNLElBQUksR0FBVyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUN2QyxJQUFNLEtBQUssR0FBVyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN6QyxJQUFNLFFBQVEsR0FBVyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUUvQyxJQUFNLEdBQUcsR0FBRyw4REFBOEQsQ0FBQztJQUMzRSwrQ0FBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxFQUFFLFVBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNO1FBQ2pFLElBQUcsS0FBSztZQUFFLE1BQU0sS0FBSyxDQUFDO1FBQ3RCLElBQUcsTUFBTTtZQUFHLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUMsT0FBTyxFQUFFLGtDQUFrQyxFQUFDLENBQUMsQ0FBQyxDQUFDO0lBRXBHLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDLENBQUM7QUFFSCxxQ0FBcUM7QUFDckMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBQyxPQUF3QixFQUFFLFFBQTBCO0lBQ3BFLElBQU0sTUFBTSxHQUFXLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQzNDLElBQU0sUUFBUSxHQUFXLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQy9DLElBQU0sWUFBWSxHQUFXLG1DQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7SUFFOUMsSUFBTSxHQUFHLEdBQUcscUNBQXFDLENBQUM7SUFDbEQsK0NBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxFQUFFLFVBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNO1FBQ2pFLElBQUcsS0FBSztZQUFFLE1BQU0sS0FBSyxDQUFDO1FBRXRCLElBQUksT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEIsS0FBb0IsVUFBTyxFQUFQLG1CQUFPLEVBQVAscUJBQU8sRUFBUCxJQUFPLEVBQUU7Z0JBQXpCLElBQU0sTUFBTTtnQkFDWixJQUFHLE1BQU0sQ0FBQyxRQUFRLEtBQUssWUFBWSxFQUFFO29CQUNqQyxPQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2lCQUNoRDthQUNKO1NBQ0o7YUFBTTtZQUNILE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUMsT0FBTyxFQUFFLGtDQUFrQyxFQUFDLENBQUMsQ0FBQztTQUN0RjtJQUNMLENBQUMsQ0FBQyxDQUFDO0FBRVAsQ0FBQyxDQUFDLENBQUM7QUFFSCwyQkFBMkI7QUFDM0IsR0FBRyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxVQUFDLE9BQXdCLEVBQUUsUUFBMEI7SUFDOUUsSUFBTSxHQUFHLEdBQUcsd0JBQXdCLENBQUM7SUFDckMsK0NBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLFVBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNO1FBQ3pDLElBQUcsS0FBSztZQUFFLE1BQU0sS0FBSyxDQUFDO1FBQ3RCLElBQUcsT0FBTztZQUFFLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDLENBQUMsQ0FBQztBQUVILHdEQUF3RDtBQUN4RCxHQUFHLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxVQUFDLE9BQXdCLEVBQUUsUUFBMEI7SUFDM0UsSUFBTSxFQUFFLEdBQVcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNsRCxJQUFNLElBQUksR0FBVyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUV2QyxJQUFNLEdBQUcsR0FBRyw2Q0FBNkMsQ0FBQztJQUMxRCwrQ0FBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsVUFBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU07UUFDcEQsSUFBRyxLQUFLO1lBQUUsTUFBTSxLQUFLLENBQUM7UUFDdEIsSUFBRyxNQUFNO1lBQUUsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBQyxPQUFPLEVBQUUsY0FBYyxFQUFDLENBQUMsQ0FBQyxDQUFDO0lBQy9FLENBQUMsQ0FBQztBQUNOLENBQUMsQ0FBQyxDQUFDO0FBRUgsR0FBRyxDQUFDLE1BQU0sQ0FBRSxJQUFJLEVBQUUsY0FBUSxPQUFPLENBQUMsR0FBRyxDQUFFLHVCQUFxQixJQUFNLENBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7QUNyRnpFLHdDOzs7Ozs7Ozs7OztBQ0FBLG9DOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLG1DIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vYXBwL3RzL3NlcnZlci50c1wiKTtcbiIsImltcG9ydCAqIGFzIE15c3FsIGZyb20gXCJteXNxbFwiO1xyXG5cclxuZXhwb3J0IGNvbnN0IGNvbm5lY3Rpb24gPSBNeXNxbC5jcmVhdGVDb25uZWN0aW9uKHtcclxuICAgIGhvc3Q6ICdsb2NhbGhvc3QnLFxyXG4gICAgdXNlcjogJ3Jvb3QnLFxyXG4gICAgcGFzc3dvcmQ6ICcnLFxyXG4gICAgZGF0YWJhc2U6ICdjb250YWN0JyxcclxuICAgIHBvcnQ6IDMzMDZcclxufSk7XHJcbiIsImltcG9ydCAqIGFzIGV4cHJlc3MgZnJvbSBcImV4cHJlc3NcIjtcclxuaW1wb3J0ICogYXMgYm9keVBhcnNlciBmcm9tICdib2R5LXBhcnNlcic7XHJcbmltcG9ydCAqIGFzIHNoYTI1NiBmcm9tICdzaGEyNTYnO1xyXG5pbXBvcnQge2Nvbm5lY3Rpb259IGZyb20gJy4vYmRkJztcclxuXHJcbmNvbnN0IHBvcnQ6IG51bWJlciA9IDkwMDA7XHJcblxyXG5jb25zdCBhcHA6IGV4cHJlc3MuQXBwbGljYXRpb24gPSBleHByZXNzKCk7XHJcblxyXG4vLyBQZXJtZXQgZGUgZ8OpcmVyIGxlcyBhY2Nlc3MgYXUgc2VydmV1cnNcclxuYXBwLnVzZShmdW5jdGlvbihyZXEsIHJlcywgbmV4dCkge1xyXG4gICAgcmVzLmhlYWRlcihcIkFjY2Vzcy1Db250cm9sLUFsbG93LU9yaWdpblwiLCBcIipcIik7XHJcbiAgICByZXMuaGVhZGVyKCdBY2Nlc3MtQ29udHJvbC1BbGxvdy1NZXRob2RzJywgJ0dFVCwgUEFUQ0gsIFBPU1QsIE9QVElPTlMnKTtcclxuICAgIHJlcy5oZWFkZXIoXCJBY2Nlc3MtQ29udHJvbC1BbGxvdy1IZWFkZXJzXCIsIFwiT3JpZ2luLCBYLVJlcXVlc3RlZC1XaXRoLCBDb250ZW50LVR5cGUsIEFjY2VwdFwiKTtcclxuICAgIC8vaW50ZXJjZXB0cyBPUFRJT05TIG1ldGhvZFxyXG4gICAgaWYgKCdPUFRJT05TJyA9PT0gcmVxLm1ldGhvZCkge1xyXG4gICAgICAgIC8vcmVzcG9uZCB3aXRoIDIwMFxyXG4gICAgICAgIHJlcy5zZW5kKDIwMCk7XHJcbiAgICB9XHJcbiAgICBlbHNlIHtcclxuICAgICAgICAvL21vdmUgb25cclxuICAgICAgICBuZXh0KCk7XHJcbiAgICB9XHJcbn0pO1xyXG5hcHAudXNlKGJvZHlQYXJzZXIudXJsZW5jb2RlZCh7ZXh0ZW5kZWQ6IHRydWV9KSk7XHJcbmFwcC51c2UoYm9keVBhcnNlci5qc29uKCkpO1xyXG5cclxuLy8gSW5zZXJ0IHVuIG5vdXZlYXUgbWVzc2FnZVxyXG5hcHAucG9zdCggJy9jb250YWN0JywgKCByZXF1ZXN0OiBleHByZXNzLlJlcXVlc3QsIHJlc3BvbnNlOiBleHByZXNzLlJlc3BvbnNlICkgPT4ge1xyXG4gICAgY29uc3QgbmFtZTogc3RyaW5nID0gcmVxdWVzdC5ib2R5Lm5hbWU7XHJcbiAgICBjb25zdCBlbWFpbDogc3RyaW5nID0gcmVxdWVzdC5ib2R5LmVtYWlsO1xyXG4gICAgY29uc3QgcXVlc3Rpb246IHN0cmluZyA9IHJlcXVlc3QuYm9keS5xdWVzdGlvbjtcclxuXHJcbiAgICBjb25zdCBzcWwgPSAnSU5TRVJUIElOVE8gbWVzc2FnZXMgKG5hbWUsIGVtYWlsLCBxdWVzdGlvbikgVkFMVUVTKD8sID8sID8pJztcclxuICAgIGNvbm5lY3Rpb24ucXVlcnkoc3FsLCBbbmFtZSwgZW1haWwsIHF1ZXN0aW9uXSwgKGVycm9yLCByZXN1bHQsIGZpZWxkcykgPT4ge1xyXG4gICAgICAgIGlmKGVycm9yKSB0aHJvdyBlcnJvcjtcclxuICAgICAgICBpZihyZXN1bHQpICByZXR1cm4gcmVzcG9uc2Uuc2VuZChKU09OLnN0cmluZ2lmeSh7XCJncmVhdFwiOiBcIlZvdHJlIG1lc3NhZ2UgYSBiaWVuIMOpdMOpIGVudm95ZXJcIn0pKTtcclxuXHJcbiAgICB9KTtcclxufSk7XHJcblxyXG4vLyBWw6lyaWZpZSBzaSBsJ3VzZXIgZXQgYmllbiBleGlzdGFudFxyXG5hcHAucG9zdCgnL2xvZ2luJywgKHJlcXVlc3Q6IGV4cHJlc3MuUmVxdWVzdCwgcmVzcG9uc2U6IGV4cHJlc3MuUmVzcG9uc2UpID0+IHtcclxuICAgIGNvbnN0IHBzZXVkbzogc3RyaW5nID0gcmVxdWVzdC5ib2R5LnBzZXVkbztcclxuICAgIGNvbnN0IHBhc3N3b3JkOiBzdHJpbmcgPSByZXF1ZXN0LmJvZHkucGFzc3dvcmQ7XHJcbiAgICBjb25zdCBwYXNzd29yZEhhc2g6IHN0cmluZyA9IHNoYTI1NihwYXNzd29yZCk7XHJcblxyXG4gICAgY29uc3Qgc3FsID0gJ1NFTEVDVCAqIEZST00gYWRtaW5zIFdIRVJFIHBzZXVkbz0/JztcclxuICAgIGNvbm5lY3Rpb24ucXVlcnkoc3FsLCBbcHNldWRvLCBwYXNzd29yZEhhc2hdLCAoZXJyb3IsIHJlc3VsdHMsIGZpZWxkcykgPT4ge1xyXG4gICAgICAgIGlmKGVycm9yKSB0aHJvdyBlcnJvcjtcclxuXHJcbiAgICAgICAgaWYgKHJlc3VsdHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBmb3IoY29uc3QgcmVzdWx0IG9mIHJlc3VsdHMpIHtcclxuICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5wYXNzd29yZCA9PT0gcGFzc3dvcmRIYXNoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLnNlbmQoSlNPTi5zdHJpbmdpZnkocmVzdWx0KSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2Uuc2VuZChKU09OLnN0cmluZ2lmeSh7XCJlcnJvclwiOiBcIm1vdCBkZSBwYXNzZSBvdSBwc2V1ZG8gaW5jb3JyZWN0XCJ9KSlcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbn0pO1xyXG5cclxuLy8gRW52b2llIHRvdXQgbGVzIG1lc3NhZ2VzXHJcbmFwcC5nZXQoJy9jb250YWN0L21lc3NhZ2VzJywgKHJlcXVlc3Q6IGV4cHJlc3MuUmVxdWVzdCwgcmVzcG9uc2U6IGV4cHJlc3MuUmVzcG9uc2UpID0+IHtcclxuICAgIGNvbnN0IHNxbCA9ICdTRUxFQ1QgKiBGUk9NIG1lc3NhZ2VzJztcclxuICAgIGNvbm5lY3Rpb24ucXVlcnkoc3FsLCAoZXJyb3IsIHJlc3VsdHMsIGZpZWxkcykgPT4ge1xyXG4gICAgICAgIGlmKGVycm9yKSB0aHJvdyBlcnJvcjtcclxuICAgICAgICBpZihyZXN1bHRzKSByZXR1cm4gcmVzcG9uc2Uuc2VuZChKU09OLnN0cmluZ2lmeShyZXN1bHRzKSk7XHJcbiAgICB9KTtcclxufSk7XHJcblxyXG4vLyBNZXRzIGEgam91ciBsZSBtZXNzYWdlIGVuIHNww6ljaWZpYW50IHF1J2lsIGEgw6l0YWl0IGx1XHJcbmFwcC5wYXRjaCgnL2NvbnRhY3QvOmlkJywgKHJlcXVlc3Q6IGV4cHJlc3MuUmVxdWVzdCwgcmVzcG9uc2U6IGV4cHJlc3MuUmVzcG9uc2UpID0+IHtcclxuICAgIGNvbnN0IGlkOiBudW1iZXIgPSBwYXJzZUludChyZXF1ZXN0LnBhcmFtc1snaWQnXSk7XHJcbiAgICBjb25zdCByZWFkOiBudW1iZXIgPSByZXF1ZXN0LmJvZHkucmVhZDtcclxuXHJcbiAgICBjb25zdCBzcWwgPSAnVVBEQVRFIG1lc3NhZ2VzIFNFVCBgcmVhZGAgPSA/IFdIRVJFIGlkID0gPyc7XHJcbiAgICBjb25uZWN0aW9uLnF1ZXJ5KHNxbCwgW3JlYWQsIGlkXSwgKGVycm9yLCByZXN1bHQsIGZpZWxkcykgPT4ge1xyXG4gICAgICAgIGlmKGVycm9yKSB0aHJvdyBlcnJvcjtcclxuICAgICAgICBpZihyZXN1bHQpIHJldHVybiByZXNwb25zZS5zZW5kKEpTT04uc3RyaW5naWZ5KHtcImdyZWF0XCI6IFwibWFpbCBjaGVja2VkXCJ9KSk7XHJcbiAgICB9KVxyXG59KTtcclxuXHJcbmFwcC5saXN0ZW4oIHBvcnQsICgpID0+IHsgY29uc29sZS5sb2coIGBMaXN0ZW5pbmcgb24gcG9ydCAke3BvcnR9YCApOyB9KTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYm9keS1wYXJzZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZXhwcmVzc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJteXNxbFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzaGEyNTZcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==