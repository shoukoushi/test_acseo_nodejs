import * as Mysql from "mysql";

export const connection = Mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'contact',
    port: 3306
});
