import * as express from "express";
import * as bodyParser from 'body-parser';
import * as sha256 from 'sha256';
import {connection} from './bdd';

const port: number = 9000;

const app: express.Application = express();

// Permet de gérer les access au serveurs
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, PATCH, POST, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        //respond with 200
        res.send(200);
    }
    else {
        //move on
        next();
    }
});
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Insert un nouveau message
app.post( '/contact', ( request: express.Request, response: express.Response ) => {
    const name: string = request.body.name;
    const email: string = request.body.email;
    const question: string = request.body.question;

    const sql = 'INSERT INTO messages (name, email, question) VALUES(?, ?, ?)';
    connection.query(sql, [name, email, question], (error, result, fields) => {
        if(error) throw error;
        if(result)  return response.send(JSON.stringify({"great": "Votre message a bien été envoyer"}));

    });
});

// Vérifie si l'user et bien existant
app.post('/login', (request: express.Request, response: express.Response) => {
    const pseudo: string = request.body.pseudo;
    const password: string = request.body.password;
    const passwordHash: string = sha256(password);

    const sql = 'SELECT * FROM admins WHERE pseudo=?';
    connection.query(sql, [pseudo, passwordHash], (error, results, fields) => {
        if(error) throw error;

        if (results.length > 0) {
            for(const result of results) {
                if(result.password === passwordHash) {
                    return response.send(JSON.stringify(result));
                }
            }
        } else {
            return response.send(JSON.stringify({"error": "mot de passe ou pseudo incorrect"}))
        }
    });

});

// Envoie tout les messages
app.get('/contact/messages', (request: express.Request, response: express.Response) => {
    const sql = 'SELECT * FROM messages';
    connection.query(sql, (error, results, fields) => {
        if(error) throw error;
        if(results) return response.send(JSON.stringify(results));
    });
});

// Mets a jour le message en spécifiant qu'il a était lu
app.patch('/contact/:id', (request: express.Request, response: express.Response) => {
    const id: number = parseInt(request.params['id']);
    const read: number = request.body.read;

    const sql = 'UPDATE messages SET `read` = ? WHERE id = ?';
    connection.query(sql, [read, id], (error, result, fields) => {
        if(error) throw error;
        if(result) return response.send(JSON.stringify({"great": "mail checked"}));
    })
});

app.listen( port, () => { console.log( `Listening on port ${port}` ); });
